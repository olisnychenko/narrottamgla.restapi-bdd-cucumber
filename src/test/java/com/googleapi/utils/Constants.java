package com.googleapi.utils;

public class Constants {	
	public static final String GOOGLE_API_KEY = "API-key";
	public static final String GOOGLE_GEOCODE_ENDPOINTS = "https://maps.googleapis.com/maps/api/geocode/json";	
}
