package com.googleapi.runners;

import cucumber.api.CucumberOptions;

@CucumberOptions(features = { "src/test/resources/features/api/googlegeo.feature" }, glue = {
		"com.googleapi.steps" })
public class GoogleGeoAPITest {
}
