Feature: Verify Geocoding and Reverse GeocodingAPI of Google 

Scenario: Verify Google Geocoding API with Valid data 
	Given The user have proper API key 
	And Have valid location address as "49 Bogart St, Brooklyn, NY 11206, USA" 
	When The user sents GET request to google Geocoding API with API key 
	Then API should return status as 200 
	And Response content type should be json 
	And Response should have result node 
	And Result should have status as "OK" 
	And Result should have formatted address as "49 Bogart St, Brooklyn, NY 11206, USA" 
	And Result should have address_components 
	And Result should have geometry 
	And Geo metry should have location lat as "40.7054136"
	And Geo metry should have location lng as "-73.9335512"
	And Geo metry should have location_type "ROOFTOP" 
	And Geo metry should view port northeast 
	And Geo metry should view port southwest 
	And Northeast should have lat as "40.7067369802915"
	And Northeast should have lng as "-73.93221776970849"
	And Southwest should have lat as "40.7040390197085"
	And Southwest should have lng as "-73.9349157302915"
	
	
Scenario: Verify Google Reverse Geocoding API with Valid data 
	Given The user have proper API key 
	And Have valid location lat as "40.7054058"
	And Have valid location lang as "-73.9335481"
	When The user sents GET request to google Reverse Geocoding API with API key 
	Then API should return status as 200 
	And Response content type should be json 
	And Response should have result node 
	And Result should have status as "OK" 
	And Result should have formatted address as "49 Bogart St, Brooklyn, NY 11206, USA"
	And Result should have address_components 
	And Result should have geometry 
	And Geo metry should have location lat as "40.7054136"
	And Geo metry should have location lng as "-73.9335512"
	And Geo metry should have location_type "ROOFTOP" 
	And Geo metry should view port northeast 
	And Geo metry should view port southwest 
	And Northeast should have lat as "40.7067625802915"
	And Northeast should have lng as "-73.93220221970849"
	And Southwest should have lat as "40.7040646197085"
	And Southwest should have lng as "-73.9349001802915"
	
	
    